/*
 * Copyright (c) 2020 com.company.workshop.service
 */
package com.company.workshop.service;

import com.company.workshop.entity.CreditRequest;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.View;
import com.haulmont.thesis.core.entity.Individual;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * @author serge
 */
@Service(CreditRequestService.NAME)
public class CreditRequestServiceBean implements CreditRequestService {
    @Inject
    protected Persistence persistence;
    @Inject
    protected Metadata metadata;
    @Override
    public Integer getCreditRequestCount(Individual individual) {
        Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            TypedQuery<CreditRequest> query = em.createQuery(
                    "select c from workshop$CreditRequest c where c.borrower.id = :borrowerId",
                    CreditRequest.class);
            query.setView(metadata.getViewRepository().getView(CreditRequest.class, View.MINIMAL));
            query.setParameter("borrowerId", individual.getId());
            List<CreditRequest> list = query.getResultList();
            tx.commit();
            if (CollectionUtils.isNotEmpty(list)) {
                return list.size();
            } else {
                return 0;
            }
        } finally {
            tx.end();
        }
    }
}