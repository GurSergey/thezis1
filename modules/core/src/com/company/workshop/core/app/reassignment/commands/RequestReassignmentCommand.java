/*
 * Copyright (c) 2020 com.company.workshop.core.app.reassignment.commands
 */
package com.company.workshop.core.app.reassignment.commands;


import com.haulmont.thesis.core.app.reassignment.commands.AbstractDocReassignmentCommand;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;

import com.company.workshop.entity.Request;

/**
 * @author serge
 */
@ManagedBean(RequestReassignmentCommand.NAME)
public class RequestReassignmentCommand extends AbstractDocReassignmentCommand<Request> {
    protected static final String NAME = "request_reassignment_command";

    @PostConstruct
    protected void postInit() {
        type = "Request";
        docQuery = String.format(DOC_QUERY_TEMPLATE, "workshop$Request");
    }

    @Override
    public String getName() {
        return NAME;
    }
}