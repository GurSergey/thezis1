create table WORKSHOP_CREDIT_BASE (
    CARD_ID uuid,
    --
    NUMBER_ varchar(50),
    BANK_ID uuid,
    TYPE_CREDIT_ID uuid,
    DATA_ date,
    AMOUNT decimal(19, 2),
    --
    -- from workshop$CreditTemplate
    TEMPLATE_NAME varchar(255),
    GLOBAL_ boolean,
    --
    primary key (CARD_ID)
);
