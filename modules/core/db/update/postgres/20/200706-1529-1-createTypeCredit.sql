create table WORKSHOP_TYPE_CREDIT (
    ID uuid,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CODE varchar(50) not null,
    TITLE varchar(50) not null,
    DESCRIPTION varchar(50),
    --
    primary key (ID)
);
