--Add default doc kind for workshop$Request
CREATE OR REPLACE FUNCTION baseInsert()
RETURNS integer
AS $$
DECLARE
cnt integer = 0;
BEGIN
cnt = (select count(CATEGORY_ID) from DF_DOC_KIND where category_id = '3f436713-9c30-44b7-afab-ba4bb781bd6d');
if(cnt = 0) then
    insert into SYS_CATEGORY (ID, NAME, ENTITY_TYPE, IS_DEFAULT, CREATE_TS, CREATED_BY, VERSION, DISCRIMINATOR)
    values ( '3f436713-9c30-44b7-afab-ba4bb781bd6d', 'Заявка на кредит', 'workshop$Request', false, now(), USER, 1, 1);

    insert into DF_DOC_KIND (category_id, create_ts, created_by, version, doc_type_id, numerator_id, 
    numerator_type, category_attrs_place, tab_name, portal_publish_allowed, disable_add_process_actors, create_only_by_template)
    values ('3f436713-9c30-44b7-afab-ba4bb781bd6d', 'now()', 'admin', 1, '1c936d84-4591-4520-9c78-c75677bb1f6b', '0752f4a9-a85e-47b7-a7da-3a327e67906e', 
    1, 1, 'Р”РѕРї. РїРѕР»СЏ', false, false, false);
end if;return 0;

END;
$$
LANGUAGE plpgsql;
^
select baseInsert();
^
drop function if exists baseInsert()^
