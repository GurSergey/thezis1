--Add default doc kind for workshop$CreditRequest
CREATE OR REPLACE FUNCTION baseInsert()
RETURNS integer
AS $$
DECLARE
cnt integer = 0;
BEGIN
cnt = (select count(CATEGORY_ID) from DF_DOC_KIND where category_id = 'b95d3217-46ff-4a50-8e1b-9d3b42b49f5b');
if(cnt = 0) then
    insert into SYS_CATEGORY (ID, NAME, ENTITY_TYPE, IS_DEFAULT, CREATE_TS, CREATED_BY, VERSION, DISCRIMINATOR)
    values ( 'b95d3217-46ff-4a50-8e1b-9d3b42b49f5b', 'Заявка на кредит', 'workshop$CreditRequest', false, now(), USER, 1, 1);

    insert into DF_DOC_KIND (category_id, create_ts, created_by, version, doc_type_id, numerator_id, 
    numerator_type, category_attrs_place, tab_name, portal_publish_allowed, disable_add_process_actors, create_only_by_template)
    values ('b95d3217-46ff-4a50-8e1b-9d3b42b49f5b', 'now()', 'admin', 1, '33e65d0e-e0f8-4eb8-8571-c07cc8cc25e1', '9dc9e5f9-5707-4ee0-bd61-f7533d336309', 
    1, 1, 'Р”РѕРї. РїРѕР»СЏ', false, false, false);
end if;return 0;

END;
$$
LANGUAGE plpgsql;
^
select baseInsert();
^
drop function if exists baseInsert()^
