--Update process card_types for entity workshop$Request
update wf_proc set card_types = regexp_replace(card_types, E',workshop\\$Request', '') where code in ('Endorsement','Resolution','Acquaintance','Registration')^
update wf_proc set updated_by='admin', card_types = card_types || 'workshop$Request,' where code in ('Endorsement','Resolution','Acquaintance','Registration')^
