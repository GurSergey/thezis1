/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import javax.persistence.PrimaryKeyJoinColumn;
import com.haulmont.cuba.core.entity.annotation.SystemLevel;
import com.haulmont.thesis.core.annotation.CardBase;
import com.haulmont.thesis.core.entity.Bank;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.haulmont.thesis.core.entity.TsCard;

/**
 * @author serge
 */
@CardBase
@SystemLevel
@PrimaryKeyJoinColumn(name = "CARD_ID", referencedColumnName = "ID")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("1")
@Table(name = "WORKSHOP_CREDIT_BASE")
@Entity(name = "workshop$CreditBase")
public class CreditBase extends TsCard {
    private static final long serialVersionUID = -5957633305251762352L;

    @Column(name = "NUMBER_", length = 50)
    protected String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BANK_ID")
    protected Bank bank;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TYPE_CREDIT_ID")
    protected TypeCredit typeCredit;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATA_", length = 50)
    protected Date data;

    @Column(name = "AMOUNT", length = 50)
    protected BigDecimal amount;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Bank getBank() {
        return bank;
    }

    public void setTypeCredit(TypeCredit typeCredit) {
        this.typeCredit = typeCredit;
    }

    public TypeCredit getTypeCredit() {
        return typeCredit;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getData() {
        return data;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }


}