/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;

import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.thesis.core.entity.Templatable;
import javax.persistence.Column;
import java.util.Locale;
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import com.haulmont.chile.core.annotations.NamePattern;

/**
 * @author serge
 */
@NamePattern("%s|templateName")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("3")
@Entity(name = "workshop$CreditTemplate")
@EnableRestore
@TrackEditScreenHistory
public class CreditTemplate extends CreditBase implements Templatable {
    private static final long serialVersionUID = 788723519798290922L;

    @Column(name = "TEMPLATE_NAME")
    protected String templateName;

    @Column(name = "GLOBAL_", length = 50)
    protected Boolean global = false;

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }


    public void setGlobal(Boolean global) {
        this.global = global;
    }

    public Boolean getGlobal() {
        return global;
    }

    @Override
    public boolean isTemplateInstance() {
        return true;
    }

    @Override
    public String getTemplatePrefix(Locale locale) {
        Messages messages = AppBeans.get(Messages.NAME);
        return messages.getMessage(getClass(), "CreditTemplate");
    }
}