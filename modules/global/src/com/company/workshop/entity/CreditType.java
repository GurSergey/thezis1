/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;


/**
 * @author serge
 */
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.NamePattern;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;

@NamePattern("%s|name")
@Table(name = "WORKSHOP_CREDIT_TYPE")
@Entity(name = "workshop$CreditType")
@EnableRestore
@TrackEditScreenHistory
public class CreditType extends StandardEntity {
    private static final long serialVersionUID = -6706012295469800860L;

    @Column(name = "NAME", nullable = false, length = 150)
    protected String name;

    @Column(name = "CODE", nullable = false, length = 3)
    protected String code;

    @Column(name = "COMMENT_", nullable = false, length = 400)
    protected String comment;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }


}