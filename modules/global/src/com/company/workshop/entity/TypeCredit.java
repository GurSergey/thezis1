/*
 * Copyright (c) 2020 com.company.workshop.entity
 */
package com.company.workshop.entity;


/**
 * @author serge
 */
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.NamePattern;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;

@NamePattern("%s|code")
@Table(name = "WORKSHOP_TYPE_CREDIT")
@Entity(name = "workshop$TypeCredit")
@EnableRestore
@TrackEditScreenHistory
public class TypeCredit extends StandardEntity {
    private static final long serialVersionUID = -6506678127780390288L;

    @Column(name = "CODE", nullable = false, length = 50)
    protected String code;

    @Column(name = "TITLE", nullable = false, length = 50)
    protected String title;

    @Column(name = "DESCRIPTION", length = 50)
    protected String description;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}