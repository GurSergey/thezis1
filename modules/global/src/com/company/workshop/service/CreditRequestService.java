/*
 * Copyright (c) 2020 com.company.workshop.service
 */
package com.company.workshop.service;

import com.haulmont.thesis.core.entity.Individual;

/**
 * @author serge
 */
public interface CreditRequestService {
    String NAME = "workshop_CreditRequestService";

    Integer getCreditRequestCount(Individual individual);

}