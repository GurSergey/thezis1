
package com.company.workshop.web.credittemplate;

import com.company.workshop.entity.CreditTemplate;
import com.company.workshop.web.credit.CreditEdit;

public class CreditTemplateEdit extends CreditEdit<CreditTemplate> {

    @Override
    protected boolean isNumberAssignNeeded() {
        return false;
    }

    @Override
    protected boolean isImportantButtonVisible() {
        return false;
    }
}