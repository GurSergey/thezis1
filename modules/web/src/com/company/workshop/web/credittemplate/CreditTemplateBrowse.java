
package com.company.workshop.web.credittemplate;

import com.company.workshop.entity.Credit;
import com.company.workshop.entity.CreditBase;
import com.company.workshop.entity.CreditTemplate;
import com.company.workshop.web.credit.CreditBrowse;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.thesis.web.ui.common.actions.CreateCardFromTemplateAction;

import javax.inject.Inject;
import java.util.Map;

public class CreditTemplateBrowse extends CreditBrowse<CreditTemplate> {

    @Inject
    protected Button createCardFromTemplateButton;
    @Inject
    protected Metadata metadata;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        initCreateCardFromTemplateAction();
    }

    protected void initCreateCardFromTemplateAction() {
        createCardFromTemplateButton.setAction(new CreateCardFromTemplateAction<CreditBase>(
                metadata.getClass(Credit.class), this, "createCardFromTemplate"));
    }

    protected boolean isCreateByTemplateActionEnabled() {
        return false;
    }

    @Override
    protected void addImportantColumn() {
    }

    @Override
    protected void addLocStateColumn() {
    }

    @Override
    protected void initCardDetailsFunctionality() {
    }

    @Override
    protected boolean isFilterImportantConditionEnabled() {
        return false;
    }
}