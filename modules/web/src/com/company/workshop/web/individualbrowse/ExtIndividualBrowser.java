/*
 * Copyright (c) 2020 com.company.workshop.web.individualbrowse
 */
package com.company.workshop.web.individualbrowse;

import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.thesis.web.ui.individual.IndividualBrowser;
import com.haulmont.cuba.gui.components.Table;

import javax.inject.Inject;
import java.util.Map;

/**
 * @author serge
 */
public class ExtIndividualBrowser extends IndividualBrowser {
    
    @Inject
    private Table individualsTable;
    
    @Override
    public void init(Map<String, Object> params) {
        individualsTable.addGeneratedColumn("someColumn", new Table.ColumnGenerator() {
            @Override
            public Component generateCell(Entity entity) {
                return new Table.PlainTextCell("123");
            }
      });
    }

}
