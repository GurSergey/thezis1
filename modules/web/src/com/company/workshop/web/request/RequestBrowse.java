package com.company.workshop.web.request;

import java.util.Map;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.thesis.web.ui.basicdoc.browse.AbstractDocBrowser;
import com.company.workshop.entity.Request;

public class RequestBrowse extends AbstractDocBrowser<Request> {

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        entityName = "workshop$Request";
    }
}